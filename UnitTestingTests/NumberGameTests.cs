﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;

namespace UnitTesting.Tests
{
    [TestClass()]
    public class NumberGameTests
    {
        [TestMethod()]
        public void RateGuessTest_returns_2()
        {
            var die = new Mock<Die>();
            die.Setup(x => x.Roll()).Returns(5);
            var numberGame = new NumberGame(die.Object);
            var result = numberGame.RateGuess(5);
            Assert.AreEqual(2, result);
        }

        [TestMethod()]
        public void RateGuessTest_returns_1()
        {
            var numberGame = new NumberGame(new DieMock(5));
            var result = numberGame.RateGuess(4);
            Assert.AreEqual(2, result);
        }

        [TestMethod()]
        public void RateGuessTest_returns_0()
        {
            var numberGame = new NumberGame(new DieMock(5));
            var result = numberGame.RateGuess(3);
            Assert.AreEqual(2, result);
        }

        [TestMethod()]
        public void Roll_Method_Rolls_Once()
        {
            var DieMock = new DieMock(5);
            var numberGame = new NumberGame(DieMock);
            var result = numberGame.RateGuess(3);
            Assert.AreEqual(1, DieMock.RollIsCalledXTimes);
        }
    }
}